
FROM ubuntu:16.04

# standard yocto depends
# from crops/ubuntu-16.04-base
# https://github.com/crops/yocto-dockerfiles/blob/master/dockerfiles/ubuntu/ubuntu-16.04/ubuntu-16.04-base/Dockerfile
RUN apt-get update && \
    apt-get install -y \
        gawk \
        wget \
        git-core \
        subversion \
        diffstat \
        unzip \
        sysstat \
        texinfo \
        gcc-multilib \
        build-essential \
        chrpath \
        socat \
        python \
        python3 \
        xz-utils  \
        locales \
        cpio \
        sudo 

# special stuff from toradex page
# https://developer.toradex.com/knowledge-base/board-support-package/openembedded-(core)#Prerequisites
RUN dpkg --add-architecture i386 && \
    apt-get update -y && \
    apt-get install -y g++-5-multilib && \
    apt-get install -y curl dosfstools gawk g++-multilib gcc-multilib lib32z1-dev libcrypto++9v5:i386 libcrypto++-dev:i386 liblzo2-dev:i386 lzop libsdl1.2-dev libstdc++-5-dev:i386 libusb-1.0-0:i386 libusb-1.0-0-dev:i386 uuid-dev:i386 texinfo chrpath && \
    apt-get install -y bash repo vim && \
    cd /usr/lib && \
    ln -s libcrypto++.so.9.0.0 libcryptopp.so.6 

# make sure the shell is bash (not dash)
RUN rm /bin/sh && \
    ln -s /bin/bash /bin/sh

# add build user
RUN useradd -U -m builder && \
    /usr/sbin/locale-gen en_US.UTF-8 && \
    apt-get clean

ENV TZ=UTC
ENV LANG=en_US.UTF-8

USER builder 
WORKDIR /home/builder
CMD /bin/bash
